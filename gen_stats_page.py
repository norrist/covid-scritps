import csv
import sys
import jinja2

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)


ALL_STATE_DATA = []
last_update = ""


def get_population(state):
    with open("population.csv", newline="") as csvfile:
        filereader = csv.DictReader(csvfile)
        for row in filereader:
            if row["State"] == state:
                try:
                    return int(row["2019_pop"].replace(",", ""))
                except ValueError:
                    pass
    return 0


def read_data(state):
    state_data = {}
    state = state.replace("_", " ")
    with open("covid-19-data/us-states.csv", newline="") as csvfile:
        filereader = csv.DictReader(csvfile)
        line_count = 0

        for row in filereader:
            if line_count == 0:
                # print(f'Column names are {", ".join(row)}')
                pass
            line_count += 1
            if row["state"] == state:

                state_data["state_name"] = state
                state_data["population"] = get_population(state)
                state_data["cases"] = int(row["cases"])
                state_data["deaths"] = int(row["deaths"])
        global last_update
        last_update = row["date"]

        return state_data


def gen_stats(state, state_data):

    cases = state_data["cases"]
    deaths = state_data["deaths"]
    population = state_data["population"]
    death_rate = deaths * 100 / cases
    state_data["death_rate"] = f"{death_rate:.3f}%"

    if population == 0:
        return
    cases_per_100k = cases * 100_000 / population
    deaths_per_100k = deaths * 100_000 / population
    state_data["cases_per_100k"] = f"{cases_per_100k:.3f}"
    state_data["deaths_per_100k"] = f"{deaths_per_100k:.3f}"
    ALL_STATE_DATA.append(state_data)


def gen_stats_page():
    TEMPLATE_FILE = "stats_page.html"
    template = templateEnv.get_template(TEMPLATE_FILE)
    outputText = template.render(ALL_STATE_DATA=ALL_STATE_DATA, last_update=last_update)
    outfile = open("output/stats.html", "w")
    outfile.write(outputText)


def gen_tables_page():
    TEMPLATE_FILE = "tables.html"
    template = templateEnv.get_template(TEMPLATE_FILE)
    outputText = template.render(ALL_STATE_DATA=ALL_STATE_DATA, last_update=last_update)
    outfile = open("output/tables.html", "w")
    outfile.write(outputText)


def gen_stats_for_list_of_states(list_of_states):
    for state in list_of_states:
        print(f"Generating stats for {state}")
        state_data = read_data(state)
        gen_stats(state, state_data)
    gen_stats_page()
    gen_tables_page()


if __name__ == "__main__":
    # print(sys.argv)
    if len(sys.argv) != 2:
        state = "Arkansas"
    else:
        state = sys.argv[1].replace("_", " ")
    gen_stats_for_list_of_states([state])
