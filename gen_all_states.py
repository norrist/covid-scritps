import csv
import gen_stats_page
import get_recent_trend_figures
from update_data import update_data_csv

# import gen_graphs


def get_list_of_states():
    states = []
    update_data_csv()
    with open("covid-19-data/us-states.csv", newline="", encoding="utf8") as csvfile:
        filereader = csv.DictReader(csvfile)
        for row in filereader:
            try:
                states.append(row["state"].replace(" ", "_"))
            except ValueError:
                pass
    return sorted(set(states))


if __name__ == "__main__":
    print(get_list_of_states())
    for state in get_list_of_states():

        # os.system(f"python3 ./gen_graphs.py {state}")

        get_recent_trend_figures.gen_recent_for_state(state, 1000, 14, "output")
        get_recent_trend_figures.gen_recent_for_state(
            state, 30, 3, "output/last_30_days"
        )

        gen_stats_page.gen_stats_for_list_of_states([state])
