import get_recent_trend_figures
import os
from urllib import request

os.makedirs("covid-19-data", exist_ok=True)
os.makedirs("output/recent", exist_ok=True)
remote_url = (
    "  https://raw.githubusercontent.com/nytimes/covid-19-data/master/us-states.csv "
)
local_file = "covid-19-data/us-states.csv"
request.urlretrieve(remote_url, local_file)


"""
curl \
  https://raw.githubusercontent.com/nytimes/covid-19-data/master/us-states.csv \
  -o covid-19-data/us-states.csv
"""


def test_gen_recent_for_state():
    get_recent_trend_figures.gen_recent_for_state("test")
