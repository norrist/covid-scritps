from os.path import exists
from os import makedirs
import requests


def update_data_csv():
    data_file = "covid-19-data/us-states.csv"
    if not exists(data_file):
        print("\nDownloading Data CSV")
        makedirs("covid-19-data", exist_ok=True)
        url = "https://raw.githubusercontent.com/nytimes/covid-19-data/master/us-states.csv"
        myfile = requests.get(url)
        open(data_file, "wb").write(myfile.content)
