git pull
curl   https://raw.githubusercontent.com/nytimes/covid-19-data/master/us-states.csv   -o covid-19-data/us-states.csv
podman build -t fedora-matplotlib .
podman run  --privileged  -v "$(pwd)":/app/ fedora-matplotlib  python3 gen_all_states.py
podman run  --privileged  -v "$(pwd)":/app/ fedora-matplotlib  python3 get_recent_trend_figures.py
./gen_html.sh

firefox output/
