import os

# import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import gen_all_states
from update_data import update_data_csv


default_range = 100
default_trend_avg = 7
default_output_dir = "output/recent"
axis_max_mult = 2  # limit axis based on average.

update_data_csv()
covid_data = pd.read_csv("covid-19-data/us-states.csv")

pd.options.mode.chained_assignment = None  # FIXME


def make_output_dir(output_dir=default_output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)


def gen_state_figure(state_latest_data, state, output_dir=default_output_dir):
    make_output_dir(output_dir)
    # fig, ax = plt.subplots()
    fig, (ax1, ax2) = plt.subplots(2, 1)
    plt.figsize = (8, 6)
    plt.suptitle(f"{state}")

    plt.subplot(211)
    ax1.plot(state_latest_data["date"], state_latest_data["new_cases"])
    ax1.plot(state_latest_data["date"], state_latest_data["new_cases_avg"])
    ax1.set_ylim(
        ymin=0, ymax=max(state_latest_data["new_cases_avg"].dropna()) * axis_max_mult
    )
    ax1.set_ylabel("Cases")

    plt.subplot(212)
    ax2.plot(state_latest_data["date"], state_latest_data["new_deaths"])
    ax2.plot(state_latest_data["date"], state_latest_data["new_deaths_avg"])
    ax2.set_ylim(
        ymin=0, ymax=max(state_latest_data["new_deaths_avg"].dropna()) * axis_max_mult
    )
    ax2.set_ylabel("Deaths")

    ax1.xaxis.set_major_locator(mdates.MonthLocator())
    ax2.xaxis.set_major_locator(mdates.MonthLocator())
    fig.autofmt_xdate()
    plt.savefig(f"{output_dir}/{state.replace(' ','_')}.png")
    plt.close()


def state_dataframe(
    state,
    tail_days=default_range,
    avg_window=default_trend_avg,
):
    state = state.replace("_", " ")
    state_covid_rows = covid_data["state"] == state
    state_data = covid_data[state_covid_rows]

    state_data["new_cases"] = state_data["cases"].diff()
    state_data["new_deaths"] = state_data["deaths"].diff()
    state_data["new_cases_avg"] = (
        state_data["new_cases"].rolling(window=avg_window).mean()
    )
    state_data["new_deaths_avg"] = (
        state_data["new_deaths"].rolling(window=avg_window).mean()
    )
    return state_data


def gen_recent_for_state(
    state,
    tail_days=default_range,
    avg_window=default_trend_avg,
    output_dir=default_output_dir,
):
    state_data = state_dataframe(state)
    state_latest_data = state_data.tail(tail_days)
    # print(state_latest_data)
    print(f"Generating {output_dir}/{state}")
    gen_state_figure(state_latest_data, state, output_dir)


def gen_all_recent():
    all_states = gen_all_states.get_list_of_states()

    for state in all_states:
        gen_recent_for_state(state)


if __name__ == "__main__":
    make_output_dir()
    gen_all_recent()
