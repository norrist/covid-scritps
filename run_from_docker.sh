git pull
mkdir -p output
curl   https://raw.githubusercontent.com/nytimes/covid-19-data/master/us-states.csv   -o covid-19-data/us-states.csv
docker build -t fedora-matplotlib .
docker run --rm --privileged  -v "$(pwd)":/app/ fedora-matplotlib  python3 gen_all_states.py
docker run --rm --privileged  -v "$(pwd)":/app/ fedora-matplotlib  python3 get_recent_trend_figures.py
./gen_html.sh
rsync -av output/ /var/www/cloud.norrist.xyz
