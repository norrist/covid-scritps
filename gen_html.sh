set -euo pipefail
IFS=$'\n\t'


tail covid-19-data/us-states.csv
ls -trl covid-19-data/us-states.csv

DATA_DATE=$(tail -n1 covid-19-data/us-states.csv |cut -f1 -d',')
cd output
echo "<H1> Data from $DATA_DATE </H1>" > index.html
for F in *png
    do
    echo "<img src=$F>"
    # cat `echo $F |sed 's/png/html/'`|| echo "No Stats"
    
done >> index.html

cd recent
echo "<H1> Data from $DATA_DATE </H1>" > index.html
for F in *png
    do
    echo "<img src=$F>"
    # cat `echo $F |sed 's/png/html/'`|| echo "No Stats"
    
done >> index.html